package com.sample.record;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.sample.record.service.ForegroundService;

public class IncomingCallReceiver extends BroadcastReceiver {

    /**
     * Properties
     */
    private static final String TAG = IncomingCallReceiver.class.getSimpleName();
    private static boolean incomingFlag = false;

    /**
     * Lifecycle
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            incomingFlag = false;
            String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            Log.i(TAG, "Call OUT:" + phoneNumber);
        } else {
            TelephonyManager tm =
                    (TelephonyManager) context.getSystemService(Service.TELEPHONY_SERVICE);
            switch (tm.getCallState()) {
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.d(TAG, "onReceive: 1");
                    incomingFlag = true;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.d(TAG, "onReceive: 2");
                    if (incomingFlag) {
                        toggleRecord(context, true);
                    }
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    new Handler(Looper.getMainLooper()).postDelayed(() -> {
                        Log.d(TAG, "onReceive: 3");
                        if (incomingFlag) {
                            incomingFlag = false;
                            toggleRecord(context, false);
                        }
                    }, 1000L);
                    break;
            }
        }
    }

    public void toggleRecord(Context context, Boolean isEnabled) {
        Intent intent = new Intent(context, ForegroundService.class);
        intent.putExtra(Constant.CString.EXTRA_RECORD, isEnabled);
        context.startForegroundService(intent);
    }
}