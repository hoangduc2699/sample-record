package com.sample.record;


public final class Constant {
// MARK: - Number constants

    /**
     * Contain all numbers in the application
     */
    public static class Number {
        public static final int FOREGROUND_NOTIFICATION_ID = 2;
        public static final int SUGGESTED_PURCHASING_COEFFICIENT = 3;
        public static final float SEARCH_POPUP_HEIGHT_PERCENT = 0.6f;
        public static final float SEARCH_POPUP_HEIGHT_OFFSET = -40f;
    }

    /**
     * String constant
     */
    public static class CString {
        public static final String EMPTY = "";
        public static final String EXTRA_PURCHASE = "EXTRA_PURCHASE";
        public static final String EXTRA_RECORD = "EXTRA_RECORD";
        public static final String NOTIFICATION_CHANNEL_ID = "CHANNEL_ID";
        public static final String NOTIFICATION_CHANNEL_NAME = "NOTIFICATION_CHANNEL_NAME";
        public static final String RECORD_DIRECTORY_NAME = "SampleRecord";
        public static final String RECORD_FILE_EXT = ".mp3";
        public static final String RECORDING_DATE_FORMAT = "yyyyMMdd_HHmm";
        public static final String RECORDING_SEPARATOR = "_";
    }

}

