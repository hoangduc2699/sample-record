package com.sample.record.ui;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;

import com.sample.record.Helper;
import com.sample.record.R;
import com.sample.record.service.ForegroundService;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Helper.hasGrantedAllPermission(this, true)) {
            ((TextView) findViewById(R.id.tv_status)).setText("OK");
            startForegroundService(new Intent(this, ForegroundService.class));
        } else {
            requestPermissions();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (Helper.hasGrantedAllPermission(this, true)) {
                ((TextView) findViewById(R.id.tv_status)).setText("OK");
                startForegroundService(new Intent(this, ForegroundService.class));
            }
        }
    }

    private void requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && !Environment.isExternalStorageManager()) {
            goToManageStorageSetting();
        } else {
            requestOtherPermissions();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    public void goToManageStorageSetting() {
        Intent myIntent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
        startActivity(myIntent);
    }

    private void requestOtherPermissions() {
        if (!Helper.hasGrantedAllPermission(this, true)) {
            if (!Helper.isAccessibilityServiceEnabled(this)) {
                Helper.requestAccessibilityService(this);
            } else {
                Helper.requestAllPermissions(this, 123, true);
            }
        } else {
            ((TextView) findViewById(R.id.tv_status)).setText("OK");
            startForegroundService(new Intent(this, ForegroundService.class));
        }
    }
}