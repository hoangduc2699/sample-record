package com.sample.record;


import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.File;
import java.util.ArrayList;


public class Helper {
    /**
     * Check if other permissions granted
     */
    public static boolean isFreePermissionsGranted(Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        for (String permission : getFreePermissions()) {
            boolean granted = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
            if (!granted) {
                return false;
            }
        }
        if (!manager.areNotificationsEnabled()) {
            return false;
        }
        return true;
    }

    /**
     * Request other permissions
     */
    private static ArrayList<String> getFreePermissions() {
        ArrayList<String> permissions = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            permissions.add(Manifest.permission.POST_NOTIFICATIONS);
        }
        return permissions;
    }

    /**
     * Request paid permissions
     */
    private static ArrayList<String> getPaidPermissions() {
        ArrayList<String> permissions = new ArrayList<>();
        permissions.add(Manifest.permission.RECORD_AUDIO);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.R) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        return permissions;
    }

    /**
     * Request paid permissions
     */
    public static void requestAllPermissions(Activity activity, int requestCode, Boolean hasPaid) {
        ArrayList<String> permissions = new ArrayList<>(getFreePermissions());
        if (hasPaid) {
            permissions.addAll(getPaidPermissions());
        }
        ActivityCompat.requestPermissions(activity, permissions.toArray(new String[0]), requestCode);
    }

    public static boolean isAccessibilityServiceEnabled(Context context) {
        String s = Settings.Secure.getString(
                context.getApplicationContext().getContentResolver(),
                Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES
        );
        try {
            return s.contains(context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void requestAccessibilityService(Context context) {
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    /**
     * Is paid permissions granted
     */
    public static boolean isPaidPermissionsGranted(Context context) {
        if (!isAccessibilityServiceEnabled(context)) {
            return false;
        }
        for (String permission : getPaidPermissions()) {
            boolean granted = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
            if (!granted) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if all permissions has been granted
     */
    public static boolean hasGrantedAllPermission(Context context, Boolean hasPaid) {
        boolean freeGranted = isFreePermissionsGranted(context);
        boolean paidGranted = !hasPaid || isPaidPermissionsGranted(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && hasPaid) {
            paidGranted = paidGranted && Environment.isExternalStorageManager();
        }
        return freeGranted && paidGranted;
    }

    /**
     * Check if service is running
     */
    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get record directory
     */
    public static String getRecordDirectory() {
        String path = Environment.getExternalStorageDirectory().getPath() + File.separator + Constant.CString.RECORD_DIRECTORY_NAME;
        File downloadFile = new File(path);
        if (!downloadFile.exists()) {
            downloadFile.mkdirs();
        }
        return path;
    }

    /**
     * Get internal record directory
     */
    public static String getInternalRecordDirectory(Context context) {
        String filePath = context.getApplicationContext().getFilesDir().getPath() + File.separator + Constant.CString.RECORD_DIRECTORY_NAME;
        File root = new File(filePath);
        if (!root.exists()) {
            root.mkdirs();
        }
        return filePath;
    }

    /**
     * Clear internal record directory
     */
    public static void clearInternalRecordDirectory(Context context) {
        File dir = new File(getInternalRecordDirectory(context));
        dir.deleteOnExit();
    }
}