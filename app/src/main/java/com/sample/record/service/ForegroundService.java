package com.sample.record.service;

import static com.sample.record.Constant.CString.NOTIFICATION_CHANNEL_ID;
import static com.sample.record.Constant.CString.NOTIFICATION_CHANNEL_NAME;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import com.sample.record.Constant;
import com.sample.record.Helper;
import com.sample.record.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ForegroundService extends Service {
    /**
     * Properties
     */
    private static final String TAG = ForegroundService.class.getSimpleName();
    MediaRecorder recorder;
    String recordingFileName;

    /**
     * Initializes
     */

    public ForegroundService() {
    }

    /**
     * Lifecycle
     */

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startMyOwnForeground();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create an instance of Window class and display the content on screen
        if (intent != null) {
            if (intent.hasExtra(Constant.CString.EXTRA_RECORD)) {
                boolean isEnabled = intent.getBooleanExtra(Constant.CString.EXTRA_RECORD, false);
                if (isEnabled) {
                    startRecording();
                } else {
                    stopRecording();
                }
            }
        }
        return START_STICKY;
    }

    /**
     * Start notification
     */
    private void startMyOwnForeground() {
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
        channel.setShowBadge(false);
        channel.setLockscreenVisibility(Notification.VISIBILITY_SECRET);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(channel);

        Notification notification = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setOngoing(true)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVisibility(NotificationCompat.VISIBILITY_SECRET)
                .setSilent(true)
                .setPriority(NotificationManager.IMPORTANCE_NONE)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(Constant.Number.FOREGROUND_NOTIFICATION_ID, notification);
    }

    /**
     * Start record
     */
    @SuppressLint("SimpleDateFormat")
    private void startRecording() {
        if (recorder != null) {
            return;
        }
        Helper.clearInternalRecordDirectory(this);
        String currentTime = new SimpleDateFormat(Constant.CString.RECORDING_DATE_FORMAT).format(Calendar.getInstance().getTime());
        recordingFileName = currentTime + Constant.CString.RECORDING_SEPARATOR + Constant.CString.RECORD_FILE_EXT;
        String filePath = Helper.getInternalRecordDirectory(this) + File.separator + recordingFileName;
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        recorder.setOutputFile(filePath);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);

        try {
            recorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        recorder.start();
    }

    /**
     * Stop record
     */
    private void stopRecording() {
        if (recorder != null) {
            recorder.release();
            recorder = null;
            try {
                exportFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void exportFile() throws IOException {
        File srcFile = new File(Helper.getInternalRecordDirectory(this), recordingFileName);
        File expFile = new File(Helper.getRecordDirectory(), recordingFileName);
        if (!expFile.exists()) {
            expFile.createNewFile();
        }
        FileChannel inChannel = null;
        FileChannel outChannel = null;
        try {
            inChannel = new FileInputStream(srcFile).getChannel();
            outChannel = new FileOutputStream(expFile).getChannel();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();
        }

    }
}
